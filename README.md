
# Test

  

## Introduccion

Por cuestiones contables el cliente nos pide hacer un mini sistema donde pueda controlar los gastos de su empresa.

El cliente requiere tener una vista donde pueda ver, editar y eliminar sus gastos en el dia.

  

## Instrucciones

  

Crear un proyecto pequeño que incluya un poco de todas las areas de desarrollo web.

  

### Backend

  Crear el backend usando laravel

 1. Agregar gasto:

	    Endpoint POST /conta/gasto
	    
	    data: { name: string, date: date(), monto: int }
	    
	    return OK/error



 1. Editar gasto:
    
	    Endpoint PUT /conta/gasto/:id
	    
	    data: { name: string, date: date(), monto: int }
	    
	    return OK/error

  

 1. Delete gasto:
        
        Endpoint DELETE /conta/gasto/:id
        return OK/error

 1. Leer gasto:
        
        Endpoint GET /conta/gastos/
		return [{ id, name, date, monto} ... {...} ]
  

## Front

  

Crear una vista usando BOOSTRAP y JS puro*, AJAX* o Angular*

*usar solo uno

  

 1. agregar GASTO:
    
    	

    Formulario con los datos a llenar, agregar SIN REFRESCAR (por eso
        el uso de JS)

	Alertas de OK or Error

 2. editar GASTO:

	Formulario con los datos a llenar, editar SIN REFRESCAR (por eso el uso de JS)

	Alertas de OK or Error

 3. Listar GASTO

	Listar en una table todos los gastos en la ultima columna poner los botones de accion de EDITAR y BORRAR

 4. delete GASTO:

	Al precionar borrar ese usuario de la vista SIN REFRESCAR (por eso el uso de JS)  

## Subir a este repositorio

   IMPORTANTE Crear una nueva RAMA

El nombre de la rama debe tener su nombre

****** NO SUBIR EN MASTER **********


